<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Events</title>
</head>
<body>
  <c:import url="header.jsp" />
  <form:form action="/events" method="post">
    <input name="eventName">
    <select name="venue">
      <c:forEach items="${venues}" var="venue">
        <option value="${venue.id.value}">${venue.name}</option>
      </c:forEach>
    </select>
    <input name="price">
    <input name="totalTickets">
    <input type="submit" value="Save">
  </form:form>
</body>
</html>