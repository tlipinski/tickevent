<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tickevent :: events</title>
</head>
<body>
  <c:import url="header.jsp" />
  <a href="/events/new">Add new event</a>
  <ul class="events">
    <c:forEach items="${events}" var="event">
      <li>
        <div>
          <a href="/events/${event.id.value}">${event.name} @ ${event.venue.name}</a>
          , price: ${event.tickets.price}, tickets: ${event.tickets.available}
          <form action="/user/cart" method="post" style="display: inline-block">
            <input type="hidden" name="eventId" value="${event.id.value}">
            <input type="submit" value="Add to cart">
          </form>
        </div>
      </li>
    </c:forEach>
  </ul>
</body>
</html>