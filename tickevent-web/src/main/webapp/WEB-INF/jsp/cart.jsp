<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cart</title>
</head>
<body>
  <c:import url="header.jsp" />
  <ul>
    <c:forEach items="${loggedInUser.cart.items}" var="item" varStatus="vs">
      <li>
        <span>Ticket for event valid until ${item.until}</span>
        <form action="/user/cart" method="post" style="display: inline-block">
          <input type="hidden" name="_method" value="delete" >
          <input type="hidden" name="itemId" value="${vs.index}">
          <input type="submit" value="Remove from cart">
        </form>
      </li>
    </c:forEach>
  </ul>
</body>
</html>