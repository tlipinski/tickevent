<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Events</title>
</head>
<body>
  <c:import url="header.jsp" />
  Event name: ${event.name}<br/>
  Price: ${event.tickets.price}
  Available tickets: ${event.tickets.available}
</body>
</html>