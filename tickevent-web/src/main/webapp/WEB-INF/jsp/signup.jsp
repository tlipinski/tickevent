<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Signup</title>
</head>
<body>
  <c:import url="header.jsp" />

  <form:form action="/users" method="post">
    <input name="email" type="email">
    <input name="password" type="password">
    <input name="passwordConfirm" type="password">
    <input type="submit" value="Sign Up">
  </form:form>
  
</body>
</html>