<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<b>Tickevent</b> :: 
<a href="/">Home</a> | 
<a href="/events">Events</a> |
<c:if test="${not empty sessionScope.loggedInUser}">
  <a href="/users/${sessionScope.loggedInUser.id.value}"> ${sessionScope.loggedInUser.email} </a> |
  <a href="/user/cart"> Cart: ${fn:length(sessionScope.loggedInUser.cart.items)} items </a> |
  <form action="/session/current" method="post" style="display: inline-block">
    <input type="hidden" name="_method" value="delete">
    <input type="submit" value="Log out">
  </form>
</c:if>
<c:if test="${empty sessionScope.loggedInUser}">
  <a href="/users/login">Log in</a> |
  <a href="/users/signup">Register</a>
</c:if>
<hr>