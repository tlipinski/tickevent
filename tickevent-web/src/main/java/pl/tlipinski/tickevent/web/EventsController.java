package pl.tlipinski.tickevent.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.tlipinski.tickevent.domain.Id;
import pl.tlipinski.tickevent.domain.ListAllVenuesReq;
import pl.tlipinski.tickevent.domain.ListAllVenuesUseCase;
import pl.tlipinski.tickevent.domain.NewEventReq;
import pl.tlipinski.tickevent.domain.NewEventRes;
import pl.tlipinski.tickevent.domain.NewEventUseCase;
import pl.tlipinski.tickevent.domain.ShowEventsReq;
import pl.tlipinski.tickevent.domain.ShowEventsRes;
import pl.tlipinski.tickevent.domain.ShowEventsUseCase;
import pl.tlipinski.tickevent.domain.ShowSingleEventReq;
import pl.tlipinski.tickevent.domain.ShowSingleEventRes;
import pl.tlipinski.tickevent.domain.ShowSingleEventUseCase;
import pl.tlipinski.tickevent.domain.Venue;

@Controller
@RequestMapping("/events")
public class EventsController {
  
  @Autowired
  private ShowEventsUseCase showEventsUseCase;

  @Autowired
  private ShowSingleEventUseCase showSingleEventUseCase;

  @Autowired
  private ListAllVenuesUseCase listAllVenuesUseCase;

  @Autowired
  private NewEventUseCase newEventUseCase;

  @RequestMapping(value = "", method = RequestMethod.GET)
  public String showAllEventsPage(ModelMap model) {
    ShowEventsRes response = showEventsUseCase.execute(new ShowEventsReq());
    model.put("events", response.events());
    return "events";
  }

  @RequestMapping(value = "/new", method = RequestMethod.GET)
  public String newEventPage(ModelMap model) {
    List<Venue> venues = listAllVenuesUseCase.execute(new ListAllVenuesReq()).venues();
    model.put("venues", venues);
    return "newEvent";
  }

  @RequestMapping(value = "", method = RequestMethod.POST)
  public String createEvent(@RequestParam String eventName, @RequestParam Long venue, @RequestParam Integer price, @RequestParam Integer totalTickets) {
    NewEventReq req = new NewEventReq(eventName, new Id(venue), price, totalTickets);
    NewEventRes res = newEventUseCase.execute(req);
    return "redirect:/events/" + res.event().getId().getValue();
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public String showEventPage(@PathVariable Long id, ModelMap model) {
    ShowSingleEventRes response = showSingleEventUseCase.execute(new ShowSingleEventReq(Id.of(id)));
    model.put("event", response.event());
    return "event";
  }

}
