package pl.tlipinski.tickevent.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.tlipinski.tickevent.domain.Id;
import pl.tlipinski.tickevent.domain.RegisterUserReq;
import pl.tlipinski.tickevent.domain.RegisterUserRes;
import pl.tlipinski.tickevent.domain.RegisterUserUseCase;
import pl.tlipinski.tickevent.domain.ShowUserReq;
import pl.tlipinski.tickevent.domain.ShowUserRes;
import pl.tlipinski.tickevent.domain.ShowUserUseCase;

@Controller
@RequestMapping("/users")
public class UsersController {
  
  @Autowired
  private RegisterUserUseCase registerUserUseCase;
  
  @Autowired
  private ShowUserUseCase showUserUseCase;
  
  @RequestMapping(value = "/signup", method = RequestMethod.GET)
  public String showSignupPage() {
    return "signup";
  }

  @RequestMapping(value = "/login", method = RequestMethod.GET)
  public String showLoginPage() {
    return "login";
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public String showUserPage(@PathVariable Long id, ModelMap model) {
    ShowUserRes res = showUserUseCase.execute(new ShowUserReq(Id.of(id)));
    model.put("user", res.user());
    return "user";
  }

  @RequestMapping(value = "", method = RequestMethod.POST)
  public String signupUser(@RequestParam String email, @RequestParam String password) {
    RegisterUserReq req = new RegisterUserReq(email, password);
    RegisterUserRes res = registerUserUseCase.execute(req);
    return "redirect:/users/" + res.user().id().value();
  }
}
