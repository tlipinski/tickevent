package pl.tlipinski.tickevent.web;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.tlipinski.tickevent.domain.AddTicketToCartReq;
import pl.tlipinski.tickevent.domain.AddTicketToCartRes;
import pl.tlipinski.tickevent.domain.AddTicketToCartUseCase;
import pl.tlipinski.tickevent.domain.Id;
import pl.tlipinski.tickevent.domain.RemoveTicketFromCartReq;
import pl.tlipinski.tickevent.domain.RemoveTicketFromCartRes;
import pl.tlipinski.tickevent.domain.RemoveTicketFromCartUseCase;
import pl.tlipinski.tickevent.domain.ShowUserReq;
import pl.tlipinski.tickevent.domain.ShowUserRes;
import pl.tlipinski.tickevent.domain.ShowUserUseCase;
import pl.tlipinski.tickevent.domain.User;

@Controller
public class CartController {
  
  @Autowired
  private AddTicketToCartUseCase addTicketToCartUseCase;
  
  @Autowired
  private RemoveTicketFromCartUseCase removeTicketFromCartUseCase;
  
  @RequestMapping(value = "/user/cart", method = RequestMethod.GET)
  public String showCart(HttpSession session, ModelMap model){
    User user = (User) session.getAttribute("loggedInUser");
    if (user == null) {
      return "redirect:/users/login";
    }
    return "cart";
  }
  
  @RequestMapping(value = "/user/cart", method = RequestMethod.POST)
  public String addNewItem(@RequestParam Long eventId, HttpSession session){
    User user = (User) session.getAttribute("loggedInUser");
    if (user == null) {
      return "redirect:/users/login";
    }
    AddTicketToCartReq req = new AddTicketToCartReq(user.id(), Id.of(eventId));
    AddTicketToCartRes res = addTicketToCartUseCase.execute(req);
    session.setAttribute("loggedInUser", res.updatedUser()); // TODO needed for current cart value, don't like this
    return "redirect:/events";
  }

  @RequestMapping(value = "/user/cart", method = RequestMethod.DELETE)
  public String removeItem(@RequestParam Integer itemId, HttpSession session){
    User user = (User) session.getAttribute("loggedInUser");
    if (user == null) {
      return "redirect:/users/login";
    }
    RemoveTicketFromCartReq req = new RemoveTicketFromCartReq(user.id(), itemId);
    RemoveTicketFromCartRes res = removeTicketFromCartUseCase.execute(req);
    session.setAttribute("loggedInUser", res.updatedUser()); 
    return "redirect:/events";
  }
}
