package pl.tlipinski.tickevent.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.tlipinski.tickevent.domain.FindVenueReq;
import pl.tlipinski.tickevent.domain.FindVenueRes;
import pl.tlipinski.tickevent.domain.FindVenueUseCase;
import pl.tlipinski.tickevent.domain.Id;
import pl.tlipinski.tickevent.domain.ListAllVenuesReq;
import pl.tlipinski.tickevent.domain.ListAllVenuesRes;
import pl.tlipinski.tickevent.domain.ListAllVenuesUseCase;

@Controller
@RequestMapping("/venues")
public class VenuesController {
  
  @Autowired
  private ListAllVenuesUseCase listAllVenuesUseCase;

  @Autowired
  private FindVenueUseCase findVenueUseCase;

  @RequestMapping(value = "", method = RequestMethod.GET)
  public String showAllEventsPage(ModelMap model) {
    ListAllVenuesRes response = listAllVenuesUseCase.execute(new ListAllVenuesReq());
    model.put("venues", response.venues());
    return "venues";
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public String showEventPage(@PathVariable Long id, ModelMap model) {
    FindVenueRes response = findVenueUseCase.execute(new FindVenueReq(Id.of(id)));
    model.put("venue", response.venue());
    return "venue";
  }

}
