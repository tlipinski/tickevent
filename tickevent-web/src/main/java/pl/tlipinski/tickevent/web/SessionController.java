package pl.tlipinski.tickevent.web;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.tlipinski.tickevent.domain.CheckUserPassReq;
import pl.tlipinski.tickevent.domain.CheckUserPassRes;
import pl.tlipinski.tickevent.domain.CheckUserPassUseCase;

@Controller
public class SessionController {
  
  @Autowired
  private CheckUserPassUseCase checkUserPassUseCase;
  
  @RequestMapping(value = "/session", method = RequestMethod.POST)
  public String createSession(@RequestParam String email, @RequestParam String password, HttpSession session) {
    CheckUserPassReq req = new CheckUserPassReq(email, password);
    CheckUserPassRes res = checkUserPassUseCase.execute(req);
    if (res.passwordValid()) {
      session.setAttribute("loggedInUser", res.user());
      return "redirect:/";
    }
    return "login";
  }

  @RequestMapping(value = "/session/current", method = RequestMethod.DELETE)
  public String deleteSession(HttpSession session) {
    session.setAttribute("loggedInUser", null);
    return "redirect:/";
  }
}
