package pl.tlipinski.tickevent.services;

import org.springframework.stereotype.Service;

import pl.tlipinski.tickevent.domain.PaymentConfirmation;
import pl.tlipinski.tickevent.domain.PaymentService;

@Service
public class FakePaymentService implements PaymentService {

  @Override
  public PaymentConfirmation confirmPayment(String cardNumber, Integer price) {
    return new PaymentConfirmation(true);
  }

}
