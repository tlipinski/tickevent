package pl.tlipinski.tickevent.storage;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Counter {

  @Id
  private String id;
  private Integer seq;

  public Counter(String id, Integer seq) {
    this.id = id;
    this.seq = seq;
  }

  public String getId() {
    return id;
  }

  public Integer getSeq() {
    return seq;
  }

}
