package pl.tlipinski.tickevent.storage.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

import pl.tlipinski.tickevent.domain.Id;

@WritingConverter
public class IdWriteConverter implements Converter<Id, Integer> {

  @Override
  public Integer convert(Id source) {
    return (int) source.getValue().intValue();
  }

}
