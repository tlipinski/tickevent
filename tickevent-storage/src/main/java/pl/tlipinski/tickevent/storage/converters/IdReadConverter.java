package pl.tlipinski.tickevent.storage.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import pl.tlipinski.tickevent.domain.Id;

@ReadingConverter
public class IdReadConverter implements Converter<Integer, Id> {

  @Override
  public Id convert(Integer source) {
    return new Id(Long.valueOf(source));
  }

}
