package pl.tlipinski.tickevent.storage;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;

public class CountersInit {
  public static void main(String[] args) {
    GenericXmlApplicationContext ctx = new GenericXmlApplicationContext("tickevent-storage-context.xml");
    MongoOperations mongo = ctx.getBean(MongoOperations.class);

    List<String> counters = Arrays.asList("event", "user", "venue");

    for (String counterName : counters) {
      if (doesNotExist(counterName, mongo)) {
        mongo.insert(new Counter(counterName, 0));
        System.out.println("Counter " + counterName + " inserted");
      }
    }
    ctx.close();
  }

  private static boolean doesNotExist(String counterName, MongoOperations mongo) {
    return !mongo.exists(query(where("_id").is(counterName)), Counter.class);
  }
}
