package pl.tlipinski.tickevent.storage;

import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import pl.tlipinski.tickevent.domain.Id;
import pl.tlipinski.tickevent.domain.Venue;
import pl.tlipinski.tickevent.domain.VenuesRepository;

@Repository
public class VenuesCollection implements VenuesRepository {

  @Autowired
  private MongoOperations mongo;

  @Override
  public List<Venue> findAll() {
    return mongo.findAll(Venue.class);
  }

  @Override
  public Venue find(Id id) {
    return mongo.findById(id.getValue(), Venue.class);
  }

  @Override
  public boolean exists(Id venueId) {
    boolean exists = mongo.exists(query(Criteria.where("_id").exists(true)), Venue.class);
    return exists;
  }

}