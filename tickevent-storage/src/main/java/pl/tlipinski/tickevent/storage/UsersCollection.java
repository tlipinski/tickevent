package pl.tlipinski.tickevent.storage;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

import pl.tlipinski.tickevent.domain.BookedTicket;
import pl.tlipinski.tickevent.domain.Cart;
import pl.tlipinski.tickevent.domain.Id;
import pl.tlipinski.tickevent.domain.User;
import pl.tlipinski.tickevent.domain.UsersRepository;

@Repository
public class UsersCollection implements UsersRepository {
  
  @Autowired
  private MongoOperations mongo;
  
  @Autowired
  private Counters counters;

  @Override
  public User get(Id id) {
    return mongo.findById(id, User.class);
  }

  @Override
  public User create(String email, String password) {
    User objectToSave = new User(Id.of(counters.getNewSeq("user")), email, password, new Cart(new ArrayList<BookedTicket>()));
    mongo.insert(objectToSave);
    return objectToSave;
  }

  @Override
  public User findByEmail(String email) {
    User user = mongo.findOne(query(where("email").is(email)), User.class);
    return user;
  }

  @Override
  public void save(User user) {
    mongo.save(user);
  }

}
