package pl.tlipinski.tickevent.storage;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import pl.tlipinski.tickevent.domain.BookedTicket;
import pl.tlipinski.tickevent.domain.BoughtTicket;
import pl.tlipinski.tickevent.domain.Event;
import pl.tlipinski.tickevent.domain.EventsRepository;
import pl.tlipinski.tickevent.domain.Id;
import pl.tlipinski.tickevent.domain.Tickets;
import pl.tlipinski.tickevent.domain.TicketsRepository;
import pl.tlipinski.tickevent.domain.Venue;

@Repository
public class EventsCollection implements EventsRepository, TicketsRepository {

  @Autowired
  private MongoOperations mongo;
  
  @Autowired
  private Counters counters;

  @Override
  public List<Event> getAllEvents() {
    return mongo.findAll(Event.class);
  }

  @Override
  public Event find(Id id) {
    return mongo.findById(id.getValue(), Event.class);
  }

  @Override
  public Event create(String eventName, Venue venue, Integer price, Integer totalTickets) {
    Long newSeq = counters.getNewSeq("event");
    Tickets tickets = new Tickets(totalTickets, price, new ArrayList<BoughtTicket>());
    Event objectToSave = new Event(new Id(newSeq), eventName, venue, tickets);
    mongo.insert(objectToSave);
    return objectToSave;
  }

  @Override
  public void save(Event event) {
    mongo.save(event);
  }

  @Override
  public List<BookedTicket> findBookedTicketsFor(Id userId) {
    List<Event> find = mongo.find(new Query(where("tickets.booked.userId").is(userId)), Event.class);
    return null;
  }

  @Override
  public void removeBookedTicket(BookedTicket ticket) {
    
  }

}