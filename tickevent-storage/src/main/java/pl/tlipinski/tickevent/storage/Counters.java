package pl.tlipinski.tickevent.storage;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class Counters {
  
  @Autowired
  private MongoOperations mongo;
  
  public Long getNewSeq(String collection) {
    Counter counter = mongo.findAndModify(query(where("_id").is(collection)), new Update().inc("seq", 1), options().returnNew(true), Counter.class);
    return (long) counter.getSeq();
  }
}
