package pl.tlipinski.tickevent.glue;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import pl.tlipinski.tickevent.domain.AddTicketToCartUseCase;
import pl.tlipinski.tickevent.domain.CheckUserPassUseCase;
import pl.tlipinski.tickevent.domain.EventsRepository;
import pl.tlipinski.tickevent.domain.FindVenueUseCase;
import pl.tlipinski.tickevent.domain.ListAllVenuesUseCase;
import pl.tlipinski.tickevent.domain.NewEventUseCase;
import pl.tlipinski.tickevent.domain.RegisterUserUseCase;
import pl.tlipinski.tickevent.domain.RemoveTicketFromCartUseCase;
import pl.tlipinski.tickevent.domain.ShowEventsUseCase;
import pl.tlipinski.tickevent.domain.ShowSingleEventUseCase;
import pl.tlipinski.tickevent.domain.ShowUserUseCase;
import pl.tlipinski.tickevent.domain.UsersRepository;
import pl.tlipinski.tickevent.domain.VenuesRepository;

@Configuration
public class UseCaseConfig {
  
  @Bean
  public ShowEventsUseCase showEventsUseCase(EventsRepository eventsRepository) {
    return new ShowEventsUseCase(eventsRepository);
  }

  @Bean
  public ShowSingleEventUseCase showSingleEventUseCase(EventsRepository eventsRepository) {
    return new ShowSingleEventUseCase(eventsRepository);
  }

  @Bean
  public ListAllVenuesUseCase listAllVenuesUseCase(VenuesRepository venuesRepository) {
    return new ListAllVenuesUseCase(venuesRepository);
  }

  @Bean
  public FindVenueUseCase findVenueUseCase(VenuesRepository venuesRepository) {
    return new FindVenueUseCase(venuesRepository);
  }
  
  @Bean
  public NewEventUseCase newEventUseCase(VenuesRepository venuesRepository, EventsRepository eventsRepository) {
    return new NewEventUseCase(eventsRepository, venuesRepository);
  }
  
  @Bean
  public RegisterUserUseCase registerUserUseCase(UsersRepository usersRpository) {
    return new RegisterUserUseCase(usersRpository);
  }

  @Bean
  public ShowUserUseCase showUserUseCase(UsersRepository usersRepository) {
    return new ShowUserUseCase(usersRepository);
  }

  @Bean
  public CheckUserPassUseCase userPassCheckUseCase(UsersRepository usersRepository) {
    return new CheckUserPassUseCase(usersRepository);
  }
  
  @Bean
  public AddTicketToCartUseCase addTicketToCartUseCase(UsersRepository usersRepository) {
    return new AddTicketToCartUseCase(usersRepository);
  }
  
  @Bean
  public RemoveTicketFromCartUseCase removeTicketFromCartUseCase(UsersRepository usersRepo) {
    return new RemoveTicketFromCartUseCase(usersRepo);
  }

}
