package pl.tlipinski.tickevent.domain

import java.util.List
import java.lang.Long
import java.lang.Integer
import scala.beans.BeanProperty
import java.util.Date
import java.util.ArrayList

case class Id(@BeanProperty value: Long)
case object Id {
  def of(value: Long) = Id(value)
}

case class Event(@BeanProperty id: Id, @BeanProperty name: String, @BeanProperty venue: Venue, @BeanProperty tickets: Tickets)

case class BookedTicket(eventId: Id, userId: Id, price: Integer, @BeanProperty until: Date)
case class BoughtTicket(eventId: Id, userId: Id, price: Integer)
case class Tickets(@BeanProperty total: Integer, @BeanProperty price: Integer, bought: List[BoughtTicket]) {
  def available = total - bought.size()
  def getAvailable = available
}

case class ShowEventsReq // TODO sorting, filtering as a param
case class ShowEventsRes(events: List[Event])

case class ShowSingleEventReq(eventId: Id)
case class ShowSingleEventRes(event: Event)

case class NewEventReq(eventName: String, venueId: Id, price: Integer, totalTickets: Integer)
case class NewEventRes(event: Event)

case class BookTicketReq(userId: Id, eventId: Id)
case class BookTicketRes(booked: Boolean, bookedTicket: BookedTicket)