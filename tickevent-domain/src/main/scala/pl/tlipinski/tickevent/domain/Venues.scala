package pl.tlipinski.tickevent.domain

import scala.beans.BeanProperty

import java.util.List
import java.lang.Long

case class Venue(@BeanProperty id: Id, @BeanProperty name: String)

case class FindVenueReq(venueId: Id)
case class FindVenueRes(venue: Venue)

case class ListAllVenuesReq
case class ListAllVenuesRes(venues: List[Venue])