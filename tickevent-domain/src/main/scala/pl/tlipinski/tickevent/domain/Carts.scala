package pl.tlipinski.tickevent.domain

import scala.beans.BeanProperty
import java.util.List

case class Cart(@BeanProperty items: List[BookedTicket])

case class AddTicketToCartReq(userId: Id, eventId: Id)
case class AddTicketToCartRes(bookedTicket: BookedTicket, updatedUser: User)

case class RemoveTicketFromCartReq(userId: Id, itemId: Integer)
case class RemoveTicketFromCartRes(updatedUser: User)