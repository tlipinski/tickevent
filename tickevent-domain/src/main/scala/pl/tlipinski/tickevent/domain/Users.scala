package pl.tlipinski.tickevent.domain

import java.util.List
import java.util.ArrayList
import scala.beans.BeanProperty

case class User(@BeanProperty id: Id, @BeanProperty email: String, @BeanProperty password: String, @BeanProperty cart: Cart)

case class RegisterUserReq(email: String, password: String)
case class RegisterUserRes(user: User)

case class ShowUserReq(id: Id)
case class ShowUserRes(user: User)

case class CheckUserPassReq(email: String, password: String)
case class CheckUserPassRes(passwordValid: Boolean, user: User)
case object CheckUserPassRes {
  def checkPassedFor(user: User) = new CheckUserPassRes(true, user)
  def checkFailed = new CheckUserPassRes(false, null)
}