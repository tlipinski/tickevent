package pl.tlipinski.tickevent.domain

import java.util.List

case class BuyBookedTicketsReq(userId: Id, cardNumber: String)
case class BuyBookedTicketsRes(boughtTickets: List[BoughtTicket])
