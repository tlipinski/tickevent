package pl.tlipinski.tickevent.domain

case class PaymentConfirmation(success: Boolean)