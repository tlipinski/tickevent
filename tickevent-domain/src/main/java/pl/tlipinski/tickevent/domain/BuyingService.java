package pl.tlipinski.tickevent.domain;

public class BuyingService {
  
  private PaymentService paymentService;

  public BoughtTicket buy(BookedTicket ticket, String cardNumber) {
    PaymentConfirmation confirmation = paymentService.confirmPayment(cardNumber, ticket.price());
    if (confirmation.success()) {
      return new BoughtTicket(ticket.eventId(), ticket.userId(), ticket.price());
    }
    return null;
  }

}
