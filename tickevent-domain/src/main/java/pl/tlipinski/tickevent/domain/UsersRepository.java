package pl.tlipinski.tickevent.domain;

public interface UsersRepository {

  User get(Id id);
  User findByEmail(String email);
  User create(String email, String password);
  void save(User userWithBooking);

}
