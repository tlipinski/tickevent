package pl.tlipinski.tickevent.domain;

import java.util.List;

public interface VenuesRepository {

  List<Venue> findAll();
  Venue find(Id id);
  boolean exists(Id venueId);

}
