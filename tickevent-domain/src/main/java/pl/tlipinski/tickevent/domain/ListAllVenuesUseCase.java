package pl.tlipinski.tickevent.domain;

import java.util.List;

public class ListAllVenuesUseCase implements UseCase<ListAllVenuesReq, ListAllVenuesRes> {
  
  private VenuesRepository venuesRepo;

  public ListAllVenuesUseCase(VenuesRepository venuesRepo) {
    this.venuesRepo = venuesRepo;
  }

  @Override
  public ListAllVenuesRes execute(ListAllVenuesReq request) {
    List<Venue> all = venuesRepo.findAll();
    return new ListAllVenuesRes(all);
  }

}
