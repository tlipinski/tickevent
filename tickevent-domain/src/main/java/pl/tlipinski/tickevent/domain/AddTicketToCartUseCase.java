package pl.tlipinski.tickevent.domain;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AddTicketToCartUseCase implements UseCase<AddTicketToCartReq, AddTicketToCartRes> {

  private UsersRepository usersRepo;

  public AddTicketToCartUseCase(UsersRepository usersRepo) {
    this.usersRepo = usersRepo;
  }

  @Override
  public AddTicketToCartRes execute(AddTicketToCartReq req) {
    BookedTicket bookedTicket = new BookedTicket(req.eventId(), req.userId(), 0, validUntilDate());

    User user = usersRepo.get(req.userId());
    user.cart().items().add(bookedTicket);
    usersRepo.save(user);

    return new AddTicketToCartRes(bookedTicket, user);
  }

  private Date validUntilDate() {
    GregorianCalendar calendar = new GregorianCalendar();
    calendar.add(Calendar.MINUTE, 10);
    Date result = calendar.getTime();
    return result;
  }
}
