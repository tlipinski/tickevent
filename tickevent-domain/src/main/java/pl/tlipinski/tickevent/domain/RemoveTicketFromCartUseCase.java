package pl.tlipinski.tickevent.domain;

public class RemoveTicketFromCartUseCase implements UseCase<RemoveTicketFromCartReq, RemoveTicketFromCartRes> {
  
  private UsersRepository usersRepo;

  public RemoveTicketFromCartUseCase(UsersRepository usersRepo) {
    this.usersRepo = usersRepo;
  }

  @Override
  public RemoveTicketFromCartRes execute(RemoveTicketFromCartReq request) {
    User user = usersRepo.get(request.userId());
    user.cart().items().remove((int) request.itemId());
    usersRepo.save(user);
    return new RemoveTicketFromCartRes(user);
  }

}
