package pl.tlipinski.tickevent.domain;

public class FindVenueUseCase implements UseCase<FindVenueReq, FindVenueRes>{
  
  private VenuesRepository venuesRepo;

  public FindVenueUseCase(VenuesRepository venuesRepo) {
    this.venuesRepo = venuesRepo;
  }

  @Override
  public FindVenueRes execute(FindVenueReq request) {
    Id id = request.venueId();
    Venue venue = venuesRepo.find(id);
    return new FindVenueRes(venue);
  }
}
