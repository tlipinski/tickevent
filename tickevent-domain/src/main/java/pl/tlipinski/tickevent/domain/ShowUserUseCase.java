package pl.tlipinski.tickevent.domain;

public class ShowUserUseCase implements UseCase<ShowUserReq, ShowUserRes> {
  
  private UsersRepository usersRepo;

  public ShowUserUseCase(UsersRepository usersRepo) {
    this.usersRepo = usersRepo;
  }

  @Override
  public ShowUserRes execute(ShowUserReq request) {
    User user = usersRepo.get(request.id());
    return new ShowUserRes(user);
  }

}
