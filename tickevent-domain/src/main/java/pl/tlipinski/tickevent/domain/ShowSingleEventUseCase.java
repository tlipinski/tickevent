package pl.tlipinski.tickevent.domain;

public class ShowSingleEventUseCase implements UseCase<ShowSingleEventReq, ShowSingleEventRes> {
  
  private EventsRepository eventsRepo;
  
  public ShowSingleEventUseCase(EventsRepository eventsRepo) {
    this.eventsRepo = eventsRepo;
  }

  @Override
  public ShowSingleEventRes execute(ShowSingleEventReq request) {
    Event event = eventsRepo.find(request.eventId());
    return new ShowSingleEventRes(event);
  }

}
