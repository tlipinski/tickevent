package pl.tlipinski.tickevent.domain;

public class ShowEventsUseCase implements UseCase<ShowEventsReq, ShowEventsRes> {
  
  private EventsRepository eventsRepo;
  
  public ShowEventsUseCase(EventsRepository eventsRepo) {
    this.eventsRepo = eventsRepo;
  }

  @Override
  public ShowEventsRes execute(ShowEventsReq request) {
    return new ShowEventsRes(eventsRepo.getAllEvents());
  }

}
