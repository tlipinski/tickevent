package pl.tlipinski.tickevent.domain;

import java.util.ArrayList;
import java.util.List;

public class BuyBookedTicketsUseCase implements UseCase<BuyBookedTicketsReq, BuyBookedTicketsRes> {
  
  private TicketsRepository ticketsRepo;
  private BuyingService buyingService;

  public BuyBookedTicketsUseCase(TicketsRepository ticketsRepo, BuyingService buyingService) {
    this.ticketsRepo = ticketsRepo;
    this.buyingService = buyingService;
  }

  @Override
  public BuyBookedTicketsRes execute(BuyBookedTicketsReq req) {
    Id userId = req.userId();
    List<BookedTicket> bookedTickets = ticketsRepo.findBookedTicketsFor(userId);
    
    List<BoughtTicket> boughtTickets = new ArrayList<>();
    for (BookedTicket ticket : bookedTickets) {
      BoughtTicket boughtTicket = buyingService.buy(ticket, req.cardNumber());
      if (boughtTicket != null) {
        boughtTickets.add(boughtTicket);
        ticketsRepo.removeBookedTicket(ticket);
      }
    }
    
    return new BuyBookedTicketsRes(boughtTickets);
  }

}
