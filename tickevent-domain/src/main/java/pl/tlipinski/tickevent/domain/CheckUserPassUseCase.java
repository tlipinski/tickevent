package pl.tlipinski.tickevent.domain;

public class CheckUserPassUseCase implements UseCase<CheckUserPassReq, CheckUserPassRes> {
  
  private UsersRepository usersRepo;

  public CheckUserPassUseCase(UsersRepository usersRepo) {
    this.usersRepo = usersRepo;
  }

  @Override
  public CheckUserPassRes execute(CheckUserPassReq request) {
    String email = request.email();
    User user = usersRepo.findByEmail(email);
    if (user.getPassword().equals(request.password())) {
      return CheckUserPassRes.checkPassedFor(user);
    } else {
      return CheckUserPassRes.checkFailed();
    }
  }

}
