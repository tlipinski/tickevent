package pl.tlipinski.tickevent.domain;

public interface UseCase<RQ, RS> {
  RS execute(RQ request);
}
