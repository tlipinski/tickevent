package pl.tlipinski.tickevent.domain;

public class RegisterUserUseCase implements UseCase<RegisterUserReq, RegisterUserRes>{
  private UsersRepository usersRepo;

  public RegisterUserUseCase(UsersRepository usersRepo) {
    this.usersRepo = usersRepo;
  }

  @Override
  public RegisterUserRes execute(RegisterUserReq req) {
    User newUser = usersRepo.create(req.email(), req.password());
    return new RegisterUserRes(newUser);
  }
  
}
