package pl.tlipinski.tickevent.domain;

public interface PaymentService {

  PaymentConfirmation confirmPayment(String cardNumber, Integer price);

}
