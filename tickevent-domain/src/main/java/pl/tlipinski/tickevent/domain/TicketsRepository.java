package pl.tlipinski.tickevent.domain;

import java.util.List;

public interface TicketsRepository {

  List<BookedTicket> findBookedTicketsFor(Id userId);
  void removeBookedTicket(BookedTicket ticket);

}
