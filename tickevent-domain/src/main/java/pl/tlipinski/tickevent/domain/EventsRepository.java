package pl.tlipinski.tickevent.domain;

import java.util.List;

public interface EventsRepository {
  List<Event> getAllEvents();

  Event find(Id id);

  Event create(String eventName, Venue venue, Integer price, Integer tickets);

  void save(Event event);
}
