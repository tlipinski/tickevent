package pl.tlipinski.tickevent.domain;

import java.util.ArrayList;

public class NewEventUseCase implements UseCase<NewEventReq, NewEventRes> {
  
  private EventsRepository eventsRepo;
  private VenuesRepository venuesRepo;

  public NewEventUseCase(EventsRepository eventsRepo, VenuesRepository venuesRepo) {
    this.eventsRepo = eventsRepo;
    this.venuesRepo = venuesRepo;
  }

  @Override
  public NewEventRes execute(NewEventReq request) {
    Id venueId = request.venueId();
    Venue venue = venuesRepo.find(venueId);
    if (venuesRepo.exists(venueId)) {
      Event createdEvent = eventsRepo.create(request.eventName(), venue, request.price(), request.totalTickets());
      return new NewEventRes(createdEvent);
    }
    
    Tickets tickets = new Tickets(0, 0, new ArrayList<BoughtTicket>());
    return new NewEventRes(new Event(Id.of(100L), "", venue, tickets)); // TODO handle failure
  }

}
